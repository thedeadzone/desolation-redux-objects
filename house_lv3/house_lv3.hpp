class DSR_Object_House_Lv3: House_F
{
	displayName = "House Level 3";
	scope = 2;
	scopeCurator = 2;
	model = "\dsr_objects\house_lv3\dsr_playerhouse_lv3.p3d";
	maximumLoad = 2000;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRHouses";
	ladders[] = { {"Start", "End" } };
	class AnimationSources
	{
		// Animation sources for doors
		class Door_1_source
		{
			source = user; // "user" = custom source = not controlled by some engine value
			initPhase = 0; // Initial value of animations based on this source
			animPeriod = 1; // Coefficient for duration of change of this animation
			sound = "GenericDoorsSound"; /// Selects sound class from CfgAnimationSourceSounds that is going to be used for sounds of doors
		};
	};
	class UserActions
	{
		class OpenDoor_1
		{
			displayNameDefault = "<img image='\A3\Ui_f\data\IGUI\Cfg\Actions\open_door_ca.paa' size='2.5' />"; // This is displayed in the center of the screen just below crosshair. In this case it's an icon, not a text.
			displayName = "Open Door"; // Label of the action used in the action menu itself.
			position = Door_1_trigger; // Point in Memory lod in p3d around which the action is available.
			priority = 0.4; // Priority coefficient used for sorting action in the action menu.
			radius = 1.5; // Range around the above defined point in which you need to be to access the action.
			onlyForPlayer = false; // Defines if the action is available only to players or AI as well.
			condition = ((this getVariable ['bis_disabled_door_1',0]) == 0) && ((this animationPhase 'Door_1_rot') < 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999); // Condition for showing the action in action menu. In this case it checks if the door is closed and if the part of the house in which the door is located hasn't been destroyed yet).
			statement = ([this, 'Door_1_rot'] call BIS_fnc_DoorNoHandleOpen); // Action taken when this action is selected in the action menu. In this case it calls a function that opens the door.
		};
		class CloseDoor_1: OpenDoor_1
		{
			displayNameDefault = "<img image='\dsr_objects\assets\close_door_ca.paa' size='2.5' />";
			displayName = "Close Door";
			priority = 0.2;
			condition = ((this animationPhase 'Door_1_rot') >= 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999); // Checks if the door is currently open and not destroyed.
			statement = ([this, 'Door_1_rot'] call BIS_fnc_DoorNoHandleClose);
		};
		class Window1Open
        {
			displayNameDefault = "<img image='\dsr_objects\assets\open_window_ca.paa' size='3.9' />";
			displayName = "Open Window";
			position = window_1_actions;
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_1l_rotation""==0 && this animationPhase ""window_1r_rotation""==0";
            statement = "this animate [""window_1l_rotation"", 1];this animate [""window_1r_rotation"", 1];";
        };
        class Window1Close
        {
            displayNameDefault = "<img image='\dsr_objects\assets\close_window_ca.paa' size='3.9' />";
            displayName = "Close Window";
            position = "window_1_actions";
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_1l_rotation""==1 && this animationPhase ""window_1r_rotation""==1";
            statement = "this animate [""window_1l_rotation"", 0];this animate [""window_1r_rotation"", 0];";
        };
        class Window2Open
        {
            displayNameDefault = "<img image='\dsr_objects\assets\open_window_ca.paa' size='3.9' />";
            displayName = "Open Window";
            position = "window_2_actions";
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_2l_rotation""==0 && this animationPhase ""window_2r_rotation""==0";
            statement = "this animate [""window_2l_rotation"", 1];this animate [""window_2r_rotation"", 1];";
        };
        class Window2Close
        {
            displayNameDefault = "<img image='\dsr_objects\assets\close_window_ca.paa' size='3.9' />";
            displayName = "Close Window";
            position = "window_2_actions";
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_2l_rotation""==1 && this animationPhase ""window_2r_rotation""==1";
            statement = "this animate [""window_2l_rotation"", 0];this animate [""window_2r_rotation"", 0];";
        };
        class Window3Open
        {
            displayNameDefault = "<img image='\dsr_objects\assets\open_window_ca.paa' size='3.9' />";
            displayName = "Open Window";
            position = "window_3_actions";
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_3l_rotation""==0 && this animationPhase ""window_3r_rotation""==0";
            statement = "this animate [""window_3l_rotation"", 1];this animate [""window_3r_rotation"", 1];";
        };
        class Window3Close
        {
            displayNameDefault = "<img image='\dsr_objects\assets\close_window_ca.paa' size='3.9' />";
            displayName = "Close Window";
            position = "window_3_actions";
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_3l_rotation""==1 && this animationPhase ""window_3r_rotation""==1";
            statement = "this animate [""window_3l_rotation"", 0];this animate [""window_3r_rotation"", 0];";
        };
        class Window4Open
        {
            displayNameDefault = "<img image='\dsr_objects\assets\open_window_ca.paa' size='3.9' />";
            displayName = "Open Window";
            position = "window_4_actions";
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_4l_rotation""==0 && this animationPhase ""window_4r_rotation""==0";
            statement = "this animate [""window_4l_rotation"", 1];this animate [""window_4r_rotation"", 1];";
        };
        class Window4Close
        {
            displayNameDefault = "<img image='\dsr_objects\assets\close_window_ca.paa' size='3.9' />";
            displayName = "Close Window";
            position = "window_4_actions";
            radius = 1.0;
            onlyForPlayer = 0;
            showWindow = 0;
            condition = "this animationPhase ""window_4l_rotation""==1 && this animationPhase ""window_4r_rotation""==1";
            statement = "this animate [""window_4l_rotation"", 0];this animate [""window_4r_rotation"", 0];";
        };
	};
	// Here are references binding specific positions in Path lod in p3d to specific actions from "class UserActions" for AI to know when to use which doors. The actionBegin# and ActionEnd# is a hardcoded naming system.
	actionBegin1 = OpenDoor_1;
	actionEnd1 = OpenDoor_1;
	actionBegin2 = OpenWindow1;
	actionEnd2 = OpenWindow1;
	actionBegin3 = OpenWindow2;
	actionEnd3 = OpenWindow2;
	actionBegin4 = OpenWindow3;
	actionEnd4 = OpenWindow3;
	actionBegin5 = OpenWindow4;
	actionEnd5 = OpenWindow4;
	// Amount of doors of this house; a parameter for easy processing of all doors on different houses by scripts.
	numberOfDoors = 5;
};
class DSR_Object_House_Lv3_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Level 3 Player House V1 Preview";
	model = "\dsr_objects\house_lv3\dsr_playerhouse_lv3_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRHouses";
	maximumLoad = 8000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_House_Lv3_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Level 3 Player House V1 Preview2";
	model = "\dsr_objects\house_lv3\dsr_playerhouse_lv3_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRHouses";
	maximumLoad = 8000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
