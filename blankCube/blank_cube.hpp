class DSR_Object_Blank_Cube: DSR_Object_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Blank Cube";
	model = "\dsr_objects\blankCube\dsr_blank.p3d";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	mass = 1;
};
