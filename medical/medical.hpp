class DSR_Object_hospital_bed_1: DSR_Object_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Hospital Bed 1";
	model = "\dsr_objects\medical\hospital_bed_1.p3d";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRProps";
};
class DSR_Object_hospital_gourney_1: DSR_Object_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Hospital Gourney 1";
	model = "\dsr_objects\medical\hospital_gourney_1.p3d";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRProps";
};
class DSR_Object_hospital_sink_1: DSR_Object_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Hospital Sink 1";
	model = "\dsr_objects\medical\hospital_sink_1.p3d";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRProps";
};