 class DSR_Object_Log_Seat: DSR_Object_Base {
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	displayName = "Log Seat";
	model = "\dsr_objects\log_seat\dsr_log_seat.p3d";
	icon = "iconObject";
	faction = "Empty";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	scope = 2;
	scopeCurator = 2;
	mass = 150;
 }; 
 class DSR_Object_Log_Seat_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Log Seat Preview";
	model = "\dsr_objects\log_seat\dsr_log_seat_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
 class DSR_Object_Log_Seat_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Log Seat Preview2";
	model = "\dsr_objects\log_seat\dsr_log_seat_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	}; 
};