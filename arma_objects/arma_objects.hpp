// A3 Objects
class DSR_Object_Fuel_Barrel: DSR_Object_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_MetalBarrel_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Fuel Barrel";
	model = "\A3\Structures_F\Items\Vessels\MetalBarrel_F.p3d";
	icon = "iconObject_circle";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	mass = 150;
	disableInventory = 1;
	transportFuel = 100;
};
class DSR_Crate_Base: NATO_Box_Base
{
	_generalMacro = "DSR_Crate_Base";
	editorPreview = "\dsr_objects\assets\logo.paa";
	model = "\A3\Structures_F\Ind\Cargo\CargoBox_V1_F.p3d";
	author = "Desolation Redux Dev Team";
	scope = 1;
	scopeCurator = 1;
	displayName = "Desolation Redux - UNDEFINED DISPLAYNAME";
	cost = 3000;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 0;
	icon = "iconObject_1x1";
	destrType = "DestructNo";
	mapSize = 1;
	mass = 150;
};
class DSR_Crate_Airdrop_F : DSR_Crate_Base
{
	_generalMacro = "DSR_Crate_Airdrop_F";
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_CargoBox_V1_F.jpg";
	author = "Desolation Redux Dev Team";
	mapSize = 1.53;
	scope = 2;
	scopeCurator = 2;
	displayName = "$STR_A3_CfgVehicles_Land_CargoBox_V1_F0";
	model = "\A3\Structures_F\Ind\Cargo\CargoBox_V1_F.p3d";
	//--- crate container restraints
	maximumLoad = 2000;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRCrates";
};
class DSR_Tent_A_F : DSR_Crate_Base
{
	_generalMacro = "DSR_Tent_A_F";
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_CargoBox_V1_F.jpg";
	author = "Desolation Redux Dev Team";
	mapSize = 1.53;
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Tent A";
	model = "\A3\Structures_F\Civ\Camping\TentA_F.p3d";
	//--- crate container restraints
	maximumLoad = 500;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRCrates";
};
class DSR_Tent_Dome_F : DSR_Crate_Base
{
	_generalMacro = "DSR_Tent_Dome_F";
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_CargoBox_V1_F.jpg";
	author = "Desolation Redux Dev Team";
	mapSize = 1.53;
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Dome Tent";
	model = "\A3\Structures_F\Civ\Camping\TentDome_F.p3d";
	//--- crate container restraints
	maximumLoad = 700;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRCrates";
};