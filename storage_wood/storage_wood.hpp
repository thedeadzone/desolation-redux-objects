class DSR_Object_Storage_Small: DSR_Crate_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Wooden crate small";
	model = "\dsr_objects\storage_wood\dsr_storage_small.p3d";
	maximumLoad = 1000;
	memoryPointSupply = "doplnovani";
	supplyRadius = 0.2;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	hiddenSelections[] = {"Camo"};                             // List of model selections which can be changed with hiddenSelectionTextures[] and hiddenSelectionMaterials[] properties. If empty, model textures are used.
    hiddenSelectionsTextures[] = {"\dsr_objects\storage_wood\data\dsr_storage_wood_co.paa"};        // The textures for the selections defined above. If empty, no texture is used.
};
class DSR_Object_Storage_Large: DSR_Crate_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Wooden crate large";
	model = "\dsr_objects\storage_wood\dsr_storage_large.p3d";
	maximumLoad = 2000;
	memoryPointSupply = "doplnovani";
	supplyRadius = 0.2;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	hiddenSelections[] = {"Camo"};                             // List of model selections which can be changed with hiddenSelectionTextures[] and hiddenSelectionMaterials[] properties. If empty, model textures are used.
    hiddenSelectionsTextures[] = {"\dsr_objects\storage_wood\data\dsr_storage_wood_co.paa"};        // The textures for the selections defined above. If empty, no texture is used.
};
class DSR_Object_Storage_Small_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Wooden crate small Preview";
	model = "\dsr_objects\storage_wood\dsr_storage_small_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	memoryPointSupply = "doplnovani";
	supplyRadius = 0.2;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Storage_Small_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Wooden crate small Preview2";
	model = "\dsr_objects\storage_wood\dsr_storage_small_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	memoryPointSupply = "doplnovani";
	supplyRadius = 0.2;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Storage_Large_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Wooden crate large Preview";
	model = "\dsr_objects\storage_wood\dsr_storage_large_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	memoryPointSupply = "doplnovani";
	supplyRadius = 0.2;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Storage_Large_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Wooden crate large Preview2";
	model = "\dsr_objects\storage_wood\dsr_storage_large_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	memoryPointSupply = "doplnovani";
	supplyRadius = 0.2;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};

