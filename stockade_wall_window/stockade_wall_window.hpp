class DSR_Object_Stockade_Wall_Window: DSR_Object_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Wall with Window";
	model = "\dsr_objects\stockade_wall_window\dsr_stockade_wall_window.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	mass = 1000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"dsr_objects\stockade_wall\data\dsr_wood_old_co.paa"
	};
	class AnimationSources
	{
		// Animation sources for doors
		class window_1_source
		{
			source = user; // "user" = custom source = not controlled by some engine value
			initPhase = 0; // Initial value of animations based on this source
			animPeriod = 1; // Coefficient for duration of change of this animation
			sound = "GenericDoorsSound"; /// Selects sound class from CfgAnimationSourceSounds that is going to be used for sounds of doors
		};
	};
	class UserActions
	{
		class Openwindow_1
		{
			displayNameDefault = "<img image='\A3\Ui_f\data\IGUI\Cfg\Actions\open_door_ca.paa' size='2.5' />"; // This is displayed in the center of the screen just below crosshair. In this case it's an icon, not a text.
			displayName = "Open Port"; // Label of the action used in the action menu itself.
			position = window_1_trigger; // Point in Memory lod in p3d around which the action is available.
			priority = 0.4; // Priority coefficient used for sorting action in the action menu.
			radius = 1.5; // Range around the above defined point in which you need to be to access the action.
			onlyForPlayer = false; // Defines if the action is available only to players or AI as well.
			condition = ((this animationPhase 'window_1_rot') < 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999); // Condition for showing the action in action menu. In this case it checks if the door is closed and if the part of the house in which the door is located hasn't been destroyed yet).
			statement = ([this, 'window_1_rot'] call BIS_fnc_DoorNoHandleOpen); // Action taken when this action is selected in the action menu. In this case it calls a function that opens the door.
		};
		class Closewindow_1: Openwindow_1
		{
			displayNameDefault = "<img image='\dsr_objects\assets\close_window_ca.paa' size='2.5' />";
			displayName = "Close Port";
			priority = 0.2;
			condition = ((this animationPhase 'window_1_rot') >= 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999); // Checks if the door is currently open and not destroyed.
			statement = ([this, 'window_1_rot'] call BIS_fnc_DoorNoHandleClose);
		};
	};
};
class DSR_Object_Stockade_Wall_Window_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Wall w/ Window Preview";
	model = "\dsr_objects\stockade_wall_window\dsr_stockade_wall_window_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Stockade_Wall_Window_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Wall w/ Window Preview2";
	model = "\dsr_objects\stockade_wall_window\dsr_stockade_wall_window_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};