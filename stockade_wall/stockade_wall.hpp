class DSR_Object_Stockade_Wall: DSR_Object_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Wall";
	model = "\dsr_objects\stockade_wall\dsr_stockade_wall.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	mass = 1000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"dsr_objects\stockade_wall\data\dsr_wood_old_co.paa"
	};
};
class DSR_Object_Stockade_Wall_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Wall Preview";
	model = "\dsr_objects\stockade_wall\dsr_stockade_wall_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Stockade_Wall_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Wall Preview2";
	model = "\dsr_objects\stockade_wall\dsr_stockade_wall_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};