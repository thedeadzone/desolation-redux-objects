class Land_FirePlace_F;
class DSR_Object_Campfire: Land_FirePlace_F
{
	scope = 2;
	model = "\dsr_objects\survival\dsr_campfire.p3d";
	icon = "iconObject_circle";
	displayName = "Campfire";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	vehicleClass = "Tents";
	destrType = "DestructNo";
	simulation = "fire";
	cost = 0;
	keepHorizontalPlacement = 0;
	coreDistance = 0.5;
	constantDistance = 0.5;
	class Effects: SmallFire
	{
		class Light1
		{
			simulation = "light";
			type = "SmallFirePlaceLight";
		};
		class sound
		{
			simulation = "sound";
			type = "Fire_camp_small";
		};
		class Smoke1
		{
			simulation = "particles";
			type = "SmallFireS";
		};
		class Fire1: Smoke1
		{
			simulation = "particles";
			type = "SmallFireF";
		};
		class Refract1
		{
			simulation = "particles";
			type = "SmallFireFRefract";
		};
	};
};
class DSR_Object_Campfire_Burning: DSR_Object_Campfire
{
	author = "Nex";
	class SimpleObject
	{
		eden = 0;
		animate[] = {};
		hide[] = {};
		verticalOffset = 0.218;
		verticalOffsetWorld = 0;
	};
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Campfire_burning_F.jpg";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Campfire Burning";
	class EventHandlers
	{
		init = "(_this select 0) inflame true";
	};
};
class DSR_Object_Campfire_Preview: DSR_Object_Campfire
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Campfire_burning_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Campfire Preview";
	model = "\dsr_objects\survival\dsr_campfire_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Campfire_Preview_2: DSR_Object_Campfire
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Campfire_burning_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Campfire Preview 2";
	model = "\dsr_objects\survival\dsr_campfire_preview_2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Campfire_Tripod: Land_FirePlace_F
{
	scope = 2;
	model = "\dsr_objects\survival\dsr_campfire_tripod.p3d";
	displayName = "Campfire w/ Cooking Pot";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	vehicleClass = "Tents";
	destrType = "DestructNo";
	simulation = "fire";
	cost = 0;
	keepHorizontalPlacement = 0;
	coreDistance = 0.5;
	constantDistance = 0.5;
	class Effects: SmallFire
	{
		class Light1
		{
			simulation = "light";
			type = "SmallFirePlaceLight";
		};
		class sound
		{
			simulation = "sound";
			type = "Fire_camp_small";
		};
		class Smoke1
		{
			simulation = "particles";
			type = "SmallFireS";
		};
		class Fire1: Smoke1
		{
			simulation = "particles";
			type = "SmallFireF";
		};
		class Refract1
		{
			simulation = "particles";
			type = "SmallFireFRefract";
		};
	};
};
class DSR_Object_Campfire_Tripod_Burning: DSR_Object_Campfire_Tripod
{
	author = "Nex";
	class SimpleObject
	{
		eden = 0;
		animate[] = {};
		hide[] = {};
		verticalOffset = 0.218;
		verticalOffsetWorld = 0;
	};
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Campfire_burning_F.jpg";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Campfire Tripod Burning";
	class EventHandlers
	{
		init = "(_this select 0) inflame true";
	};
};
class DSR_Object_Campfire_Tripod_Preview: DSR_Object_Campfire_Tripod
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Campfire_burning_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Campfire Tripod Preview";
	model = "\dsr_objects\survival\dsr_campfire_tripod_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Campfire_Tripod_Preview_2: DSR_Object_Campfire_Tripod
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Campfire_burning_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "DSR Campfire Tripod Preview 2";
	model = "\dsr_objects\survival\dsr_campfire_tripod_preview_2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};