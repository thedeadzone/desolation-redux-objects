class DSR_Object_Stockade_Tower: DSR_Object_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Tower";
	model = "\dsr_objects\stockade_tower\dsr_stockade_tower.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	ladders[] = { {"Start", "End" } };
	mass = 1500;
};
class DSR_Object_Stockade_Tower_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Tower Preview";
	model = "\dsr_objects\stockade_tower\dsr_stockade_tower_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Stockade_Tower_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Tower Preview2";
	model = "\dsr_objects\stockade_tower\dsr_stockade_tower_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};