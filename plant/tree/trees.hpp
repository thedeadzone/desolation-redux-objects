class t_picea2m: DSR_Object_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Picea2m";
	destrType="DestructTree";
	model = "\dsr_objects\plant\tree\t_picea2m.p3d";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
};