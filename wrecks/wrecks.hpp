class DSR_Object_Wreck1: DSR_Crate_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "Skodovka Wreck";
	model = "\dsr_objects\wrecks\dsr_skodovka_wrecked.p3d";
	maximumLoad = 300;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRWrecks";
};
class DSR_Object_Wreck2: DSR_Crate_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "UAZ Wreck";
	model = "\dsr_objects\wrecks\dsr_uaz_wreck.p3d";
	maximumLoad = 300;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRWrecks";
};
class DSR_Object_Wreck3: DSR_Crate_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "UH1H Wreck";
	model = "\dsr_objects\wrecks\dsr_uh1h_wreck.p3d";
	maximumLoad = 300;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRWrecks";
};
class DSR_Object_Wreck4: DSR_Crate_Base
{
	scope = 2;
	scopeCurator = 2;
	displayName = "UH1Y Wreck";
	model = "\dsr_objects\wrecks\dsr_uh1y_crashed.p3d";
	maximumLoad = 300;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRWrecks";
};