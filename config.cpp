/*
 * Desolation Redux
 * http://desolationredux.com/
 * © 2016 Desolation Dev Team
 * 
 * This work is licensed under the Arma Public License Share Alike (APL-SA) + Bohemia monetization rights.
 * To view a copy of this license, visit:
 * https://www.bistudio.com/community/licenses/arma-public-license-share-alike/
 * https://www.bistudio.com/monetization/
 */
class CfgPatches
{
	class dsr_objects
	{
		fileName = "dsr_objects.pbo";
		requiredAddons[] = {"A3_Data_F","A3_Weapons_F","A3_Structures_F_Ind", "A3_Structures_F_Civ"};
		requiredVersion = 0.1;
		units[] = {};
		weapons[] = {};
		vehicles[] = {};
		author = "Desolation Redux Dev Team";
	};
};

class CfgEditorCategories
{
    class DSRCategory
    {
        displayName = "DesolationRedux Pack";
    };
};


class CfgEditorSubcategories
{
	class DSRObjects
	{
		displayName = "DSR Objects";
	};
	class DSRCrates
	{
		displayName = "DSR Crates";
	};
	class DSRWrecks
	{
		displayName = "DSR Wrecks";
	};
	class DSRWalls
	{
		displayName = "DSR Walls";
	};
	class DSRHouses
	{
		displayName = "DSR Houses";
	};
	class DSRProps
	{
		displayName = "DSR Props";
	};
};





class SmallFire;
class CfgVehicles
{
	class House;
	class House_F: House {};
	class ReammoBox;
	class LootWeaponHolder: ReammoBox
	{
		scope = 1;
		scopeCurator = 0;
		author = "Desolation Redux Dev Team";
		isGround = 1;
		accuracy = 0.2;
		forceSupply = 1;
		showWeaponCargo = 1;
		transportMaxMagazines = 1e+009;
		transportMaxWeapons = 1e+009;
		transportMaxBackpacks = 0;
		icon = "iconObject_1x1";
		model = "\A3\Weapons_f\dummyweapon.p3d";
		destrType = "DestructNo";
		class TransportMagazines {};
	};
	class NATO_Box_Base;
	class NonStrategic;
	class DSR_Object_Base: NonStrategic
	{
		scope = 1;
		author = "Desolation Redux Dev Team";
		mapSize = 1;
		armor = 10000;
		cost = 1000;
		icon = "iconObject_1x1";
		picture = "";
		destrType = "DestructNo";
		hiddenSelections[] = {};
		hiddenSelectionsTextures[] = {};
		editorcategory = "DSRCategory";
		editorSubcategory = "DSRObjects";
	};

	
	#include "arma_objects\arma_objects.hpp"
	#include "gun_cab\gun_cab.hpp"
	#include "house_lv1\house_lv1.hpp"
	#include "house_lv1_2\house_lv1_2.hpp"
	#include "house_lv2\house_lv2.hpp"
	#include "house_lv2_2\house_lv2_2.hpp"
	#include "house_lv3\house_lv3.hpp"
	#include "decor\decor.hpp"
	#include "cot\cot.hpp"
	#include "stockade_gate\stockade_gate.hpp"
	#include "stockade_rampart\stockade_rampart.hpp"
	#include "stockade_tower\stockade_tower.hpp"
	#include "stockade_wall\stockade_wall.hpp"
	#include "stockade_wall_window\stockade_wall_window.hpp"
	#include "storage_wood\storage_wood.hpp"
	#include "workbench\workbench.hpp"
	#include "water_catchment\water_catchment.hpp"
	#include "farming\farming.hpp"
	#include "anvillog\anvillog.hpp"
	#include "billboard\billboard.hpp"
	#include "wrecks\wrecks.hpp"
	#include "bbq\bbq.hpp"
	#include "sleeping_mat\sleeping_mat.hpp"
	#include "storage_shed\storage_shed.hpp"
	#include "survival\survival.hpp"
	#include "plant\tree\trees.hpp"
	#include "blankCube\blank_cube.hpp"
	#include "medical\medical.hpp"
};