class DSR_Object_Storage_Shed : House_F
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	model = "dsr_objects\storage_shed\dsr_storageshed.p3d";
	displayName = "Storage Shed";
	icon = "iconObject_4x1";
	maximumLoad = 2000;
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	mass = 150;
/*	class CfgAnimationSourceSounds
	{
		class DesolationReduxGateRotation
		{
			class DoorMovement
			{
				loop 			= 0;
				terminate 		= 1;
				trigger 		= "(phase factor[0.05,0.10]) * (phase factor[0.95,0.9])";
				sound0[] 		= {"dsr_objects\storage_shed\sounds\desolatingaterotationsound.ogg",0.5,1.0};
				sound[] 		= {"sound0",1};
			};
		};
	};*/
	class AnimationSources
	{
		class DoorRotation
		{
			source 		= user;
			initPhase 	= 0;
			animPeriod 	= 4;
			sound 		= "GenericDoorsSound";
		};
	};
	class UserActions
	{
		/*class GateOpen
		{
			displayNameDefault = "<img image='\A3\Ui_f\data\IGUI\Cfg\Actions\open_door_ca.paa' size='2.5' />";
			displayName = "Open Gate";
			position = "Door_1_trigger";
			radius = 2;
			onlyForPlayer = 0;
			showWindow = 0;
			condition = "this animationPhase ""gate_left_rotation""==0 && this animationPhase ""gate_right_rotation""==0";
			statement = "this animate [""gate_left_rotation"", 1];this animate [""gate_right_rotation"", 1];";
		};
		class GateClose
		{
			displayNameDefault = "<img image='\dsr_objects\storage_shed\UI\close_door_ca.paa' size='2.5' />";
			displayName = "Close Gate";
			position = "door_1_trigger";
			radius = 2;
			onlyForPlayer = 0;
			showWindow = 0;
			condition = "this animationPhase ""gate_left_rotation""==1 && this animationPhase ""gate_right_rotation""==1";
			statement = "this animate [""gate_left_rotation"", 0];this animate [""gate_right_rotation"", 0];";
		};*/
		class GateOpen
		{
			displayNameDefault = "<img image='\A3\Ui_f\data\IGUI\Cfg\Actions\open_door_ca.paa' size='2.5' />"; // This is displayed in the center of the screen just below crosshair. In this case it's an icon, not a text.
			displayName = "Open Door"; // Label of the action used in the action menu itself.
			position = Door_1_trigger; // Point in Memory lod in p3d around which the action is available.
			priority = 0.4; // Priority coefficient used for sorting action in the action menu.
			radius = 2; // Range around the above defined point in which you need to be to access the action.
			onlyForPlayer = false; // Defines if the action is available only to players or AI as well.
			condition = ((this getVariable ['bis_disabled_door_1',0]) == 0) && ((this animationPhase 'gate_left_rotation') < 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999); // Condition for showing the action in action menu. In this case it checks if the door is closed and if the part of the house in which the door is located hasn't been destroyed yet).
			statement = ([this, 'gate_left_rotation'] call BIS_fnc_DoorNoHandleOpen); // Action taken when this action is selected in the action menu. In this case it calls a function that opens the door.
		};
		class GateClose: GateOpen
		{
			displayName = "Close Door";
			priority = 0.2;
			condition = ((this animationPhase 'gate_left_rotation') >= 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999); // Checks if the door is currently open and not destroyed.
			statement = ([this, 'gate_left_rotation'] call BIS_fnc_DoorNoHandleClose);
		};
		class GateOpen_2: GateOpen
		{
			position = Door_1_trigger;
			condition = ((this getVariable ['bis_disabled_door_2',0]) == 0) && ((this animationPhase 'gate_right_rotation') < 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999);
			statement = ([this, 'gate_right_rotation'] call BIS_fnc_DoorNoHandleOpen);
		};
		class GateClose_2: GateClose
		{
			position = Door_1_trigger;
			condition = ((this animationPhase 'gate_right_rotation') >= 0.5) && ((this animationPhase 'Hitzone_2_hide') < 0.99999);
			statement = ([this, 'gate_right_rotation'] call BIS_fnc_DoorNoHandleClose);
		};
	};
};
class DSR_Object_Storageshed_Preview: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Gate Preview";
	model = "\dsr_objects\storage_shed\dsr_storageshed_preview.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};
class DSR_Object_Storageshed_Preview2: DSR_Crate_Base
{
	editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_Axe_F.jpg";
	scope = 2;
	scopeCurator = 2;
	displayName = "Stockade Gate Preview2";
	model = "\dsr_objects\storage_shed\dsr_storageshed_preview2.p3d";
	icon = "iconObject_4x1";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	maximumLoad = 5000;
	hiddenSelections[] = {"camo1"}; ///we want to allow changing the color of this selection
	hiddenSelectionsTextures[]=
	{
		"#(argb,2,2,1)color(0.7,0.93,0,0.6)"
	};
};  