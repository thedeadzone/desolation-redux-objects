class DSR_Object_BBQ: House_F
{
	scope = 2;
	model = "\dsr_objects\bbq\dsr_bbq.p3d";
	displayName = "Selfmade BBQ";
	editorcategory = "DSRCategory";
	editorSubcategory = "DSRObjects";
	destrType = "DestructNo";
	simulation = "fire";
	cost=0;
	keepHorizontalPlacement=0;
	class Effects: SmallFire
	{
		class Light1
		{
			simulation = "light";
			type = "SmallFireLight";
		};
		class sound
		{
			simulation = "sound";
			type = "Fire_camp";
		};
		class Smoke1
		{
			simulation = "particles";
			type = "SmallFireS";
		};
		class Fire1: Smoke1
		{
			simulation = "particles";
			type = "SmallFireF";
		};
		class Refract1
		{
			simulation = "particles";
			type = "SmallFireFRefract";
		};
	};
};